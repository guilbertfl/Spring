<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
<title>Accueil</title>
<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">
<meta name="Content-Language" content="fr" />
<meta name="Description" content="" />
<meta name="Keywords" content=" Projet POKEMON : Spring Hibernate Bootstrap" />
<meta name="Subject" content="" />
<meta name="Content-Type" content="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">

  	
<spring:url value="/css/bootstrap-theme.min.css" var="bootstrapThemeCss" />
<spring:url value="/css/bootstrap.min.css" var="bootstrapCss" />

<spring:url value="/css/style.css" var="styleCss" />
<spring:url value="/css/footer.css" var="footerCss" />
<spring:url value="/css/header.css" var="headerCss" />

<spring:url value="/css/inscription.css" var="inscriptionCss" />
 
<spring:url value="/js/bootstrap.js" var="bootstrapJs" />
<spring:url value="/js/jquery-3.1.1.min.js" var="jqueryJs" />

<script src="${jqueryJs}"></script>
<script src="${bootstrapJs}"></script>
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${bootstrapThemeCss}" rel="stylesheet" />


<link href="${styleCss}" rel="stylesheet" />
<link href="${footerCss}" rel="stylesheet" />
<link href="${headerCss}" rel="stylesheet" />
<link href="${inscriptionCss}" rel="stylesheet" />



</head>


<body>

<jsp:include page="/WEB-INF/jsp/common/header.jsp"/>

<div class="panel panel-primary formulaire_inscription">
	<div class="panel-heading panel-heading-custom">Connexion</div>
	
	<div class="panel-body">
	
			<form:form method="POST" action="${pageContext.request.contextPath}/compte/SeConnecter" modelAttribute="dresseur">
				<div class="form-group row">
		             <form:label path="name" class="col-sm-2 col-form-label">Nom de dresseur</form:label>
		             <div class="col-sm-10">
		             	<form:input path="name" class="form-control"/>
		             </div>
		        </div>
		        
		       	<div class="form-group row">
		             <form:label path="password" class="col-sm-2 col-form-label">Password</form:label>
		             <div class="col-sm-10">
		             	<form:input path="password" type="password"  class="form-control"/>
		             </div>
		        </div>
		        
		       	<div class="form-group">
		             <input type="submit" value="Se connecter" class="btn btn-default"/>
		        </div>      
	        </form:form>
        </div>

</div>

		
<jsp:include page="/WEB-INF/jsp/common/footer.jsp"/>
	
</body>


</html>