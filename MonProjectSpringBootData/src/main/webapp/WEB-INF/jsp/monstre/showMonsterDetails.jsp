<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<!DOCTYPE html>
<html>
<head>
<title>Caractéristiques d'un Pokemon</title>

<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">
<meta name="Content-Language" content="fr" />
<meta name="Description" content="" />
<meta name="Keywords" content="Projet POKEMON : Spring Hibernate Bootstrap" />
<meta name="Subject" content="" />
<meta name="Content-Type" content="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<spring:url value="/css/bootstrap-theme.min.css" var="bootstrapThemeCss" />
<spring:url value="/css/bootstrap.min.css" var="bootstrapCss" />

<spring:url value="/css/style.css" var="styleCss" />
<spring:url value="/css/footer.css" var="footerCss" />
<spring:url value="/css/header.css" var="headerCss" />

<spring:url value="/js/bootstrap.min.js" var="bootstrapJs" />

<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${bootstrapThemeCss}" rel="stylesheet" />
<link href="${styleCss}" rel="stylesheet" />
<link href="${footerCss}" rel="stylesheet" />
<link href="${headerCss}" rel="stylesheet" />

<link href="${inscriptionCss}" rel="stylesheet" />

</head>


<body>

	<jsp:include page="../common/header.jsp" />
<body>

 <div class="container">

		<c:if test="${not empty msgAlert}">
			<div class="alert alert-${typeAlert} alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<strong>${msgAlert}</strong>
			</div>
		</c:if>

		
 <c:choose>  <%-- Debut c:choose  --%>
		<c:when test="${empty monsterDetails}">
			<div class="alert alert-warning alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
				<strong>Aucun pokemon trouvé ! What a hackerman you are !</strong>
			</div>
		</c:when>
		
 <c:otherwise>  <%-- Debut c:otherwise  --%>
		
		<div class="row">
		<div class="col-md-7">
			<h1>Détails du Pokémon</h1>
		</div>
		</div>
		
		<br/><br/>

	<div id="pokemon">
			
		<div id="detailsPrincipaux" class="panel, panel-primary">
			<div class="panel, panel-heading panel-heading-custom">Caractéristiques</div>
			
			<div class="detailsPrincipauxInt">
				<p><img src="https://www.pokebip.com/pokedex/images/sugimori/${monsterDetails.id}.png" alt ="Image de ${monsterDetails.name}"></p>
				<p><b>${monsterDetails.name}</b></p>
				
				<table id="pokedex_table" class="table table-hover table-bordered" style="background:white">
					<tr>
						<th>
							<b>Type(s) :</b>
						</th>
						<td>
							${monsterDetails.type1} ${monsterDetails.type2}
						</td>
					</tr>
					<tr>
						<th>
							<b>Taille :</b>
						</th>
						<td>
							${monsterDetails.height} m
						</td>
					</tr>
					<tr>
						<th>
							<b>Poids :</b>
						</th>
						<td>
							${monsterDetails.weight} kg
						</td>
					</tr>
					<tr>
						<th>
							<b>Capacités :</b>
						</th>
						<td>
							${monsterDetails.ability1.name} ${monsterDetails.ability2.name} ${monsterDetails.ability3.name}
						</td>
					</tr>
					<tr>
						<th>
							<b>Groupe d'oeuf :</b>
						</th>
						<td>
							${monsterDetails.egggroups}
						</td>
					</tr>
					<tr>
						<th>
							<b>Nombre de pas :</b>
						</th>
						<td>
							${monsterDetails.hatchsteps}
						</td>
					</tr>
					<tr>
						<th>
							<b>Habitat :</b>
						</th>
						<td>
							${monsterDetails.habitat.name}
						</td>
					</tr>	
					<tr>
						<th>
							<b>Evolution(s) ?</b>
						</th>
						<td>
							<p><c:forEach var="evolutions" items="${monsterDetails.evolutions}"><p><a href="/monstre/get/${evolutions.id}">${evolutions.name}</a></p></c:forEach>
						</td>
					</tr>
				</table>
			</div>
		</div>
			
			
		<div class="panel, panel-primary" id="descriptionEntete">
			<div class="panel-heading panel-heading-custom">Description</div>
			<div class="panel-body panel-body-back">
				${monsterDetails.description}
			</div>
		</div>	
		
	</div>
			
		<div class="panel, panel-primary" id="attaquesEntete">
			<div class="panel-heading panel-heading-custom">Attaques</div>
			<div class="panel-body panel-body-back">
					<c:forEach var="attaques" items="${monsterDetails.moves}"><p><a href="/attaque/get/${attaques.id}">${attaques.name}</a></p></c:forEach>
			</div>
		</div>
		


</c:otherwise> <%-- Fin c:otherwise  --%>
</c:choose> <%-- Fin c:choose  --%>
</div> <%-- Fin div class="container" --%>

<jsp:include page="../common/footer.jsp" />

</body>
</html>