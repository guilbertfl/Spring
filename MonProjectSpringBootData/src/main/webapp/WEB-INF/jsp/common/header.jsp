<header>
<nav class="navbar navbar-inverse navbar-fixed-top">
	<div>
		<div>	 

		<ul id="menu">
			<li><a href="${pageContext.request.contextPath}/"><h4>Accueil</h4></a></li>
			<li class="dropdown"><a data-toogle="dropdown" class="dropdown-toggle" href="${pageContext.request.contextPath}/monstre/listAll"><h4>Pok�dex</h4></a>
			</li>
			<li class="dropdown"><a data-toogle="dropdown" class="dropdown-toggle" href="${pageContext.request.contextPath}/attaque/listAll"><h4>Attaques</h4></a>
			</li> 
			<li class="dropdown"><a data-toogle="dropdown" class="dropdown-toggle" href="${pageContext.request.contextPath}/capacite/listAll"><h4>Capacit�s</h4></a>
			</li>
			<li class="dropdown"><a data-toogle="dropdown" class="dropdown-toggle" href="#"><h4>Compte<b class="caret"></b></h4></a>
				<ul class="dropdown-menu">
					<li><a href="${pageContext.request.contextPath}/compte/connexion">Connexion</a></li>
					<li><a href="${pageContext.request.contextPath}/compte/pagePerso">Aller � mon compte</a></li>
					<li><a href="${pageContext.request.contextPath}/compte/inscription">Inscription</a></li>
				</ul>
			</li>

			<% if (session.getAttribute("nameDresseur") == null) { %>
						
			<li class="dropdown"><a href="/compte/connexion/"><h5>
				Connexion
				</h5></a>
			</li>
			
			<li class="dropdown"><a href="/compte/inscription/"><h5>
				Inscription
				</h5></a>
			</li>
			
			<% } else { %>
			
			<li class="dropdown"><a href="/compte/pagePerso/"><h5>
				Bonjour, ${models.dresseur.name}
				</h5></a>
			</li>
			
			<li class="dropdown"><a href="/compte/deco/"><h5>
				D�connexion
				</h5></a>
			</li>

			<% } %>
			
 		</ul>	
		 
	 </div>
   </div>  
  </nav>
 </header>