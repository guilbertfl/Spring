<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
<title>Accueil</title>
<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">
<meta name="Content-Language" content="fr" />
<meta name="Description" content="" />
<meta name="Keywords" content=" Projet POKEMON : Spring Hibernate Bootstrap" />
<meta name="Subject" content="" />
<meta name="Content-Type" content="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">

  	
<spring:url value="/css/bootstrap-theme.min.css" var="bootstrapThemeCss" />
<spring:url value="/css/bootstrap.min.css" var="bootstrapCss" />

<spring:url value="/css/style.css" var="styleCss" />
<spring:url value="/css/footer.css" var="footerCss" />
<spring:url value="/css/header.css" var="headerCss" />

<spring:url value="/css/inscription.css" var="inscriptionCss" />
 
<spring:url value="/js/bootstrap.js" var="bootstrapJs" />
<spring:url value="/js/jquery-3.1.1.min.js" var="jqueryJs" />

<script src="${jqueryJs}"></script>
<script src="${bootstrapJs}"></script>
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${bootstrapThemeCss}" rel="stylesheet" />

<link href="${inscriptionCss}" rel="stylesheet" />
<link href="${styleCss}" rel="stylesheet" />
<link href="${footerCss}" rel="stylesheet" />
<link href="${headerCss}" rel="stylesheet" />




</head>


<body>

<jsp:include page="/WEB-INF/jsp/common/header.jsp"/>

<div class="panel panel-primary panel_taille">
	<div class="panel-heading panel-heading-custom">
		Mon compte
	</div>
	
	<div class="panel-body">
			Bonjour, ${models.dresseur.name}
     </div>
     
     <div class="panel, panel-primary" id="descriptionEntete">
		<div class="panel-heading panel-heading-custom">Vos pokemons</div>
			<div class="panel-body panel-body-back">
			
			<table id="pokedex_table" class="table table-hover table-bordered" style="background:white">
				<tr>
					<th>
						Pokemon 1 : 
					</th>
					
						<c:catch var="exception">
							<td>
								${models.dresseur.poke1.name}
							</td>
					<td>
							<a class="btn btn-danger" href="${pageContext.request.contextPath}/pokemon/delete/1">Supprimer ce pokemon</a>
						</c:catch>
					
						<c:if test="${exception != null}">
							Pas de pokemon dans cet emplacement
						</c:if>
					</td>
				</tr>
				<tr>
					<th>
						Pokemon 2 : 
					</th>
					
						<c:catch var="exception">
							<td>
								${models.dresseur.poke2.name}
							</td>
					<td>
							<a class="btn btn-danger" href="${pageContext.request.contextPath}/pokemon/delete/1">Supprimer ce pokemon</a>
						</c:catch>
					
						<c:if test="${exception != null}">
							Pas de pokemon dans cet emplacement
						</c:if>
					</td>
				</tr>
				<tr>
					<th>
						Pokemon 3 : 
					</th>
					
						<c:catch var="exception">
							<td>
								${models.dresseur.poke3.name}
							</td>
					<td>
							<a class="btn btn-danger" href="${pageContext.request.contextPath}/pokemon/delete/1">Supprimer ce pokemon</a>
						</c:catch>
					
						<c:if test="${exception != null}">
							Pas de pokemon dans cet emplacement
						</c:if>
					</td>
				</tr>
				<tr>
					<th>
						Pokemon 4 : 
					</th>
					
						<c:catch var="exception">
							<td>
								${models.dresseur.poke4.name}
							</td>
					<td>
							<a class="btn btn-danger" href="${pageContext.request.contextPath}/pokemon/delete/1">Supprimer ce pokemon</a>
						</c:catch>
					
						<c:if test="${exception != null}">
							Pas de pokemon dans cet emplacement
						</c:if>
					</td>
				</tr>
				<tr>
					<th>
						Pokemon 5 : 
					</th>
					
						<c:catch var="exception">
							<td>
								${models.dresseur.poke5.name}
							</td>
					<td>
							<a class="btn btn-danger" href="${pageContext.request.contextPath}/pokemon/delete/1">Supprimer ce pokemon</a>
						</c:catch>
					
						<c:if test="${exception != null}">
							Pas de pokemon dans cet emplacement
						</c:if>
					</td>
				</tr>
				<tr>
					<th>
						Pokemon 6 : 
					</th>
					
						<c:catch var="exception">
							<td>
								${models.dresseur.poke6.name}
							</td>
					<td>
							<a class="btn btn-danger" href="${pageContext.request.contextPath}/pokemon/delete/1">Supprimer ce pokemon</a>
						</c:catch>
					
						<c:if test="${exception != null}">
							Pas de pokemon dans cet emplacement
						</c:if>
					</td>
				</tr>
			</table>
		</div>
	</div>
				
		<form method="POST" action="${pageContext.request.contextPath}/pokemon/addPokemon">
				
     	<div class="panel, panel-primary" id="descriptionEntete">
			<div class="panel-heading panel-heading-custom">
				Liste des Pokémons
			</div>
		
			<div class="panel-body panel-body-back">
				<div class="col-sm-10">
					<select id="select" name="idPokedex">
						<c:forEach var="poke" items="${models.pokemons}">
							<option value="${poke.id}">${poke.name}</option> 
		             	</c:forEach>
					</select>
		        </div>
		    </div>
		</div>
		             
		<div class="panel, panel-primary" id="descriptionEntete">
		    <div class="panel-heading panel-heading-custom">
		        Emplacement dans l'équipe
		    </div>
		           
		    <div class="panel-body panel-body-back">
				<div class="col-sm-10">
					<select name="emplacement">
						<option value="1">1</option>
		             	<option value="2">2</option>
		             	<option value="3">3</option>
		             	<option value="4">4</option>
		             	<option value="5">5</option>
		             	<option value="6">6</option>
		             </select>
		        </div>
		    </div>
		       
		    <div class="form-group">
				<input type="submit" value="Ajouter ce pokemon à cet emplacement" class="btn btn-default"/>
			</div>  
		</div>  
		   
	    </form>
	</div>


		
<jsp:include page="/WEB-INF/jsp/common/footer.jsp"/>
	
</body>


</html>