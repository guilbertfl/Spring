<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
<title>Accueil</title>
<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">
<meta name="Content-Language" content="fr" />
<meta name="Description" content="" />
<meta name="Keywords" content=" Projet Spring Hibernate Bootstrap" />
<meta name="Subject" content="" />
<meta name="Content-Type" content="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">

  	
<spring:url value="/css/bootstrap-theme.min.css" var="bootstrapThemeCss" />
<spring:url value="/css/bootstrap.min.css" var="bootstrapCss" />

<spring:url value="/css/style.css" var="styleCss" />
<spring:url value="/css/footer.css" var="footerCss" />
<spring:url value="/css/header.css" var="headerCss" />
<spring:url value="/css/404.css" var="errorCss" />
 
<spring:url value="/js/bootstrap.js" var="bootstrapJs" />
<spring:url value="/js/jquery-3.1.1.min.js" var="jqueryJs" />


<script src="${jqueryJs}"></script>
<script src="${bootstrapJs}"></script>

<link href="${styleCss}" rel="stylesheet" />
<link href="${footerCss}" rel="stylesheet" />
<link href="${headerCss}" rel="stylesheet" />
<link href="${errorCss}" rel="stylesheet" />
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${bootstrapThemeCss}" rel="stylesheet" />



</head>


<body>

<jsp:include page="/WEB-INF/jsp/common/header.jsp"/>


<div class="error">
	bnhgftdfghyjunhg
</div>

		
<jsp:include page="/WEB-INF/jsp/common/footer.jsp"/>
	
</body>


</html>