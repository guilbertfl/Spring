package fr.eisti.cergy.jee.service.pokemons;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;


import fr.eisti.cergy.jee.model.Pokemons;

public interface PokemonsRepository extends Repository <Pokemons, Long>{
	
	Pokemons findById(Long id);

	
	List<Pokemons> findAll();
	
	Pokemons save (Pokemons pokemon);	
	
	void delete (Pokemons pokemon);
	
	 @Query(value = "SELECT POKEMON_SEQ.nextval FROM dual", nativeQuery = true)
		 Long getNextSerieId();
}
