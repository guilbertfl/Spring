package fr.eisti.cergy.jee.contoller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.eisti.cergy.jee.model.Dresseur;
import fr.eisti.cergy.jee.model.Monsters;
import fr.eisti.cergy.jee.model.Pokemons;
import fr.eisti.cergy.jee.service.MonsterService;
import fr.eisti.cergy.jee.service.Dresseur.DresseurService;
import fr.eisti.cergy.jee.service.pokemons.PokemonService;

@Controller("dresseurController")
@RequestMapping(value="/compte")
public class DresseurController {
	
	@Autowired
	DresseurService dresseurService;
	@Autowired
	PokemonService pokemonService;
	@Autowired
	MonsterService monsterService;
	
		

	@RequestMapping(value="/inscription", method= RequestMethod.GET)
	public String inscription(Model model) throws Exception {
		Dresseur dresseur = new Dresseur();
		model.addAttribute("dresseur", dresseur);
		return "inscription";
	}  
	
	@RequestMapping(value="/connexion", method= RequestMethod.GET)
	public String connexion(Model model) throws Exception {
		Dresseur dresseur = new Dresseur();
		model.addAttribute("dresseur", dresseur);
		return "connexion";
	} 
	
	
	
	@RequestMapping(value="/pagePerso", method= RequestMethod.GET)
	public ModelAndView authenticated(HttpSession session) throws Exception {
		if( session.getAttribute("nameDresseur") == null){
			return new ModelAndView("connexion");
		}
		
		Dresseur dresseur = dresseurService.getByName((String) session.getAttribute("nameDresseur"));
		ArrayList<Monsters> pokemons = monsterService.getAll();
		
		Map<String, Object> models = new HashMap<String, Object>();
		models.put("dresseur", dresseur);
		models.put("pokemons", pokemons);

		return new ModelAndView("pagePerso", "models", models);

	} 
	
	
	
	@RequestMapping(value="/SeConnecter", method=RequestMethod.POST)
	public String seConnecter(@ModelAttribute("dresseur")Dresseur dresseur, Model model, HttpSession session, final RedirectAttributes redirectAttributes) throws Exception {
		try{
			List<Dresseur> dresseurList = dresseurService.getAll();
		    ListIterator<Dresseur> it = dresseurList.listIterator();
		    while(it.hasNext()){
		         Dresseur i_dresseur = it.next();
		         if(i_dresseur.getName().equals(dresseur.getName()) && i_dresseur.getPassword().equals(dresseur.getPassword())){
		        	 session.setAttribute("nameDresseur", dresseur.getName());
		        	 return "redirect:/compte/pagePerso";
		         }
		            
		    }
			Long id = dresseurService.save(dresseur);

			
			
		}catch(Exception e){
			e.printStackTrace();
			return "redirect:/compte/failed";
		}
		
		return "";
		
	}
	
	@RequestMapping(value="/addDresseur", method=RequestMethod.POST)
	public String addDresseur(@ModelAttribute("dresseur")Dresseur dresseur, Model model, final RedirectAttributes redirectAttributes) throws Exception {
		try{
			
			System.out.println(dresseur.toString());
			List<Dresseur> dresseurList = dresseurService.getAll();
		    ListIterator<Dresseur> it = dresseurList.listIterator();
		    while(it.hasNext()){
		         Dresseur i_dresseur = it.next();
		         if(i_dresseur.getName().equals(dresseur.getName())){
		        	 return "redirect:/compte/failedName";
		         }
		            
		    }
			Long id = dresseurService.save(dresseur);

			return "redirect:/compte/success";
			
		}catch(Exception e){
			e.printStackTrace();
			return "redirect:/compte/failed";
		}
	}
	
	
	@RequestMapping(value="/success")
	public String afficherSuccess(Model model){
		return "inscriptionSuccess";
	}
	
	@RequestMapping(value="/failed")
	public String afficherFailed(Model model){
		return "inscriptionFailed";
	}
	
	@RequestMapping(value="/failedName")
	public String afficherFailedName(Model model){
		return "inscriptionFailedName";
	}
	
	@RequestMapping(value="/deco")
	public String deconnexion(Model model, HttpSession session){
		session.removeAttribute("nameDresseur");
		session.removeAttribute("");
		return "/";
	}
	
}
