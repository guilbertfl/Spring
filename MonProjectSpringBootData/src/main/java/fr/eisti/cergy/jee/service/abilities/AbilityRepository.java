package fr.eisti.cergy.jee.service.abilities;

import java.util.List;

import org.springframework.data.repository.Repository;

import fr.eisti.cergy.jee.model.Abilities;

public interface AbilityRepository extends Repository<Abilities, Long>{
	List<Abilities> findAll();
}
