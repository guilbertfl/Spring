package fr.eisti.cergy.jee.service;

import java.util.List;

import org.springframework.data.repository.Repository;

import fr.eisti.cergy.jee.model.Monsters;

public interface MonstersRepository extends Repository<Monsters, Long>{

	List<Monsters> findAll();
	Monsters findById(long id);
}
