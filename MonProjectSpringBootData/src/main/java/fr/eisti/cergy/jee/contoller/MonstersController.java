package fr.eisti.cergy.jee.contoller;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import fr.eisti.cergy.jee.model.Monsters;
import fr.eisti.cergy.jee.service.MonsterService;
 
@Controller("monstersController")
public class MonstersController {
	
	private final Logger logger = LoggerFactory.getLogger(MonstersController.class);

 
	@Autowired
	MonsterService monsterService;
 

	@RequestMapping(value = "/monstre/listAll", method = RequestMethod.GET)
	protected ModelAndView showAllMonsters() throws Exception {
		/*
		 * Lancement du Service et recupeation donnees en base
		 */
		ArrayList<Monsters> listeMonsters = monsterService.getAll();
		/*
		 * Envoi Vue + Modele MVC pour Affichage donnees vue
		 */
		return new ModelAndView("/monstre/showAllMonsters", "monsters", listeMonsters);
	}
	
	@RequestMapping(value = "/monstre/get/{id}", method = RequestMethod.GET)
	protected ModelAndView showMonsterDetails(@PathVariable String id) throws Exception{
		Monsters pokemonChoisi = null;
		try{
			pokemonChoisi = monsterService.getMonsterById(Long.parseLong(id));
		}catch(NullPointerException | NumberFormatException e){
		}
		return new ModelAndView("/monstre/showMonsterDetails","monsterDetails",pokemonChoisi);
	}
	
	@RequestMapping(value = "/monstre/get", method = RequestMethod.GET)
	protected ModelAndView showMonsterDetails() throws Exception{
		return showMonsterDetails("");
	}
}
