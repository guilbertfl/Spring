package fr.eisti.cergy.jee.service.moves;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.eisti.cergy.jee.model.*;

@Service
@Transactional
public interface MoveService {
	
	ArrayList<Moves> getAll();
	Moves getMoveById(long id);
}
