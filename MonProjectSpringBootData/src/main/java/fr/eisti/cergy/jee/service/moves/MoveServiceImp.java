package fr.eisti.cergy.jee.service.moves;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.eisti.cergy.jee.model.*;
import fr.eisti.cergy.jee.service.moves.MoveService;

@Service
@Transactional
public class MoveServiceImp implements MoveService{
	
	@Autowired
	private MovesRepository moveRepository;

	@Override
	public ArrayList<Moves> getAll(){
		return (ArrayList<Moves>) moveRepository.findAll();
	}

	@Override
	public Moves getMoveById(long id) {
		return (Moves) moveRepository.findById(id);
	}
}
