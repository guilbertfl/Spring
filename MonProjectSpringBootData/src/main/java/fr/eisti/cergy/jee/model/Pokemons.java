package fr.eisti.cergy.jee.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "POKEMONS", schema = "POKEISTI")
public class Pokemons {
	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="POKEMON_SEQ")
	@SequenceGenerator(name="POKEMON_SEQ", sequenceName="POKEMON_SEQ", allocationSize=1)
	private long id;
	private long dresseurid;
	private String name;
	private String type1;
	private String type2;
	private long hp;
	private long attack;
	private long defense;
	private long speed;
	private long specialAttack;
	private long specialDefense;
	
	private Abilities ability1;
	private Abilities ability2;
	private Abilities ability3;
	private Moves moveId1;
	private Moves moveId2;
	private Moves moveId3;
	private Moves moveId4;

	
	
	public Pokemons(long id, long dresseurid, String name, String type1, String type2, long hp, long attack,
			long defense, long speed, long specialAttack, long specialDefense, Abilities ability1, Abilities ability2,
			Abilities ability3, Moves moveId1, Moves moveId2, Moves moveId3, Moves moveId4) {
		super();
		this.id = id;
		this.dresseurid = dresseurid;
		this.name = name;
		this.type1 = type1;
		this.type2 = type2;
		this.hp = hp;
		this.attack = attack;
		this.defense = defense;
		this.speed = speed;
		this.specialAttack = specialAttack;
		this.specialDefense = specialDefense;
		this.ability1 = ability1;
		this.ability2 = ability2;
		this.ability3 = ability3;
		this.moveId1 = moveId1;
		this.moveId2 = moveId2;
		this.moveId3 = moveId3;
		this.moveId4 = moveId4;
	}
	
	public Pokemons(){}


	@Id

	@Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
	public long getId() {
		return id;
	}
	
	
	public void setId(long id) {
		this.id = id;
	}
	
	@Column(name = "DRESSEURID", unique = true, nullable = false, precision = 10, scale = 0)
	public long getDresseurid() {
		return dresseurid;
	}
	
	
	public void setDresseurid(long dresseurid) {
		this.dresseurid = dresseurid;
	}
	
	@Column(name = "NAME", nullable = false)
	public String getName() {
		return name;
	}
	
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "TYPE1", nullable = false)
	public String getType1() {
		return type1;
	}
	
	
	public void setType1(String type1) {
		this.type1 = type1;
	}
	
	@Column(name = "TYPE2", nullable = false)
	public String getType2() {
		return type2;
	}
	
	
	public void setType2(String type2) {
		this.type2 = type2;
	}
	
	@Column(name = "hp", unique = true, nullable = false, precision = 10, scale = 0)
	public long getHp() {
		return hp;
	}
	
	
	public void setHp(long hp) {
		this.hp = hp;
	}
	
	
	@Column(name = "ATTACK", unique = true, nullable = false, precision = 10, scale = 0)
	public long getAttack() {
		return attack;
	}
	
	
	public void setAttack(long attack) {
		this.attack = attack;
	}
	
	@Column(name = "DEFENSE", unique = true, nullable = false, precision = 10, scale = 0)
	public long getDefense() {
		return defense;
	}
	
	
	public void setDefense(long defense) {
		this.defense = defense;
	}
	
	@Column(name = "SPEED", unique = true, nullable = false, precision = 10, scale = 0)
	public long getSpeed() {
		return speed;
	}
	
	
	public void setSpeed(long speed) {
		this.speed = speed;
	}
	
	@Column(name = "SPECIALATTACK", unique = true, nullable = false, precision = 10, scale = 0)
	public long getSpecialAttack() {
		return specialAttack;
	}
	
	
	public void setSpecialAttack(long specialAttack) {
		this.specialAttack = specialAttack;
	}
	
	@Column(name = "SPECIALDEFENSE", unique = true, nullable = false, precision = 10, scale = 0)
	public long getSpecialDefense() {
		return specialDefense;
	}
	
	
	public void setSpecialDefense(long specialDefense) {
		this.specialDefense = specialDefense;
	}
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ABILITYID1", nullable = false)
	public Abilities getAbility1() {
		return this.ability1;
	}

	public void setAbility1(Abilities abilityid1) {
		this.ability1 = abilityid1;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ABILITYID2", nullable = false)
	public Abilities getAbility2() {
		return this.ability2;
	}

	public void setAbility2(Abilities ability2) {
		this.ability2 = ability2;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ABILITYID3", nullable = false)
	public Abilities getAbility3() {
		return this.ability3;
	}

	public void setAbility3(Abilities ability3) {
		this.ability3 = ability3;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MOVEID1", nullable = false)
	public Moves getMoveId1() {
		return moveId1;
	}
	
	
	public void setMoveId1(Moves moveId1) {
		this.moveId1 = moveId1;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MOVEID2", nullable = false)
	public Moves getMoveId2() {
		return moveId2;
	}
	
	
	public void setMoveId2(Moves moveId2) {
		this.moveId2 = moveId2;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MOVEID3", nullable = false)
	public Moves getMoveId3() {
		return moveId3;
	}
	
	
	public void setMoveId3(Moves moveId3) {
		this.moveId3 = moveId3;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MOVEID4", nullable = false)
	public Moves getMoveId4() {
		return moveId4;
	}
	
	
	public void setMoveId4(Moves moveId4) {
		this.moveId4 = moveId4;
	}

	@Override
	public String toString() {
		return "Pokemons [id=" + id + ", dresseurid=" + dresseurid + ", name=" + name + ", type1=" + type1 + ", type2="
				+ type2 + ", hp=" + hp + ", attack=" + attack + ", defense=" + defense + ", speed=" + speed
				+ ", specialAttack=" + specialAttack + ", specialDefense=" + specialDefense + ", ability1=" + ability1
				+ ", ability2=" + ability2 + ", ability3=" + ability3 + ", moveId1=" + moveId1 + ", moveId2=" + moveId2
				+ ", moveId3=" + moveId3 + ", moveId4=" + moveId4 + "]";
	}
	

	 
}
 