package fr.eisti.cergy.jee.service.abilities;

import java.util.ArrayList;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.eisti.cergy.jee.model.Abilities;

@Service
@Transactional
public interface AbilityService {
	ArrayList<Abilities> getAll();
}
