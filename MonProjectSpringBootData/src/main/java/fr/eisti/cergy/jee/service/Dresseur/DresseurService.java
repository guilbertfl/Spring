package fr.eisti.cergy.jee.service.Dresseur;

import java.util.List;

import fr.eisti.cergy.jee.model.Dresseur;

public interface DresseurService {
	
	public Long save (Dresseur dresseur) throws Exception ;
	
	List<Dresseur> getAll();
	
	Dresseur getById(Long id) throws Exception;
	
	Dresseur getByName(String name) throws Exception;


}
