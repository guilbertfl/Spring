package fr.eisti.cergy.jee.contoller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.eisti.cergy.jee.model.*;
import fr.eisti.cergy.jee.service.MonsterService;
import fr.eisti.cergy.jee.service.Dresseur.DresseurService;
import fr.eisti.cergy.jee.service.pokemons.PokemonService;


@Controller("pokemonController")
@RequestMapping(value="/pokemon")
public class PokemonsController {
	
	@Autowired
	PokemonService pokemonService;
	@Autowired
	MonsterService monsterService;
	@Autowired
	DresseurService dresseurService;
	
	
	@RequestMapping(value = "/delete/{s_id}", method = RequestMethod.GET)
	protected String showMonsterDetails(@PathVariable String s_id, HttpSession session) throws Exception{
		int i_id = Integer.parseInt(s_id);
		
		Dresseur dresseur = dresseurService.getByName((String) session.getAttribute("nameDresseur"));
		
		switch (i_id) {
		case 1:
			pokemonService.delete(dresseur.getPoke1());
			break;
			
		case 2:
			pokemonService.delete(dresseur.getPoke2());
			break;
			
		case 3:
			pokemonService.delete(dresseur.getPoke3());
			break;
			
		case 4:
			pokemonService.delete(dresseur.getPoke4());
			break;
			
		case 5:
			pokemonService.delete(dresseur.getPoke5());
			break;

		default:
			pokemonService.delete(dresseur.getPoke6());
			break;
		}
		
		return "redirect:/compte/pagePerso";
	}
	
		

	@RequestMapping(value="/addPokemon", method= RequestMethod.POST)
	public String ajouterUnPokemon(@RequestParam("idPokedex") String idPokedex, @RequestParam("emplacement") String emplacement, HttpSession session) throws Exception {
		Long idPoke = Long.parseLong(idPokedex);
		Dresseur dresseur = dresseurService.getByName((String) session.getAttribute("nameDresseur"));
		Monsters pokedex = monsterService.getMonsterById(idPoke);
		
		Pokemons pokemon = new Pokemons();
		pokemon.setDresseurid(dresseur.getId());
		pokemon.setId(pokemonService.getSequenceId() + 1);
		
		pokemon.setAbility1(pokedex.getAbility1());
		pokemon.setAbility2(pokedex.getAbility2());
		pokemon.setAbility3(pokedex.getAbility3());
		pokemon.setAttack(pokedex.getBaseattack());
		pokemon.setDefense(pokedex.getBasedefense());
		pokemon.setHp(pokedex.getBasehp());
		pokemon.setSpeed(pokedex.getBasespeed());
		pokemon.setSpecialAttack(pokedex.getBasespecialattack());
		pokemon.setSpecialDefense(pokedex.getBasespecialdefense());
		pokemon.setName(pokedex.getName());
		pokemon.setType1(pokedex.getType1());
		pokemon.setType2(pokedex.getType2());
		
		Set<Moves> moves = pokedex.getMoves();
		Iterator<Moves> it = moves.iterator();
		
		pokemon.setMoveId1(it.next());
		pokemon.setMoveId2(it.next());
		pokemon.setMoveId3(it.next());
		pokemon.setMoveId4(it.next());
		
		pokemonService.save(pokemon);
		
		int emp = Integer.parseInt(emplacement);
		
		switch (emp) {
			
		case 1:
			try{
				dresseur.getPoke1();
				pokemonService.delete(dresseur.getPoke1());
			}catch(Exception e){
				System.out.println("poke non existant dans"+ emp);
			}
			dresseur.setPoke1(pokemon);
			break;
		
		case 2:
			try{
				dresseur.getPoke2();
				pokemonService.delete(dresseur.getPoke2());
			}catch(Exception e){
				System.out.println("poke non existant dans"+ emp);
			}
			dresseur.setPoke2(pokemon);
			break;
			
		case 3:
			try{
				dresseur.getPoke3();
				pokemonService.delete(dresseur.getPoke3());
			}catch(Exception e){
				System.out.println("poke non existant dans"+ emp);
			}
			dresseur.setPoke3(pokemon);
			break;
			
		case 4:
			try{
				dresseur.getPoke4();
				pokemonService.delete(dresseur.getPoke4());
			}catch(Exception e){
				System.out.println("poke non existant dans"+ emp);
			}
			dresseur.setPoke4(pokemon);
			break;
			
		case 5:
			try{
				dresseur.getPoke5();
				pokemonService.delete(dresseur.getPoke5());
			}catch(Exception e){
				System.out.println("poke non existant dans"+ emp);
			}
			dresseur.setPoke5(pokemon);
			break;
			
		default:
			try{
				dresseur.getPoke6();
				pokemonService.delete(dresseur.getPoke6());
			}catch(Exception e){
				System.out.println("poke non existant dans"+ emp);
			}
			dresseur.setPoke6(pokemon);
			break;

		}
		
		dresseurService.save(dresseur);
		return "redirect:/compte/pagePerso";
	}  
	
}
