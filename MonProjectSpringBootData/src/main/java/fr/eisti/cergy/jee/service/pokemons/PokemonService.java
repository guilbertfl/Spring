package fr.eisti.cergy.jee.service.pokemons;

import java.util.ArrayList;
import java.util.List;

import fr.eisti.cergy.jee.model.Dresseur;
import fr.eisti.cergy.jee.model.Pokemons;

public interface PokemonService {
	
	public Long save (Pokemons pokemon) throws Exception ;
	
	ArrayList<Pokemons> getAll();
	
	Pokemons getById(Long id) throws Exception;

	void delete (Pokemons pokemon) throws Exception;
	
	Long getSequenceId();
}
