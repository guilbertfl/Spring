package fr.eisti.cergy.jee.service.abilities;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.eisti.cergy.jee.model.Abilities;

@Service
@Transactional
public class AbilityServiceImp implements AbilityService{
	
	@Autowired
	private AbilityRepository abilityRepository;

	@Override
	public ArrayList<Abilities> getAll(){
		return (ArrayList<Abilities>) abilityRepository.findAll();
	}
}
