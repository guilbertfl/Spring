package fr.eisti.cergy.jee.service;
import java.util.ArrayList;

import fr.eisti.cergy.jee.model.*;

public interface MonsterService {

	ArrayList<Monsters> getAll();
	Monsters getMonsterById(long id);
}
