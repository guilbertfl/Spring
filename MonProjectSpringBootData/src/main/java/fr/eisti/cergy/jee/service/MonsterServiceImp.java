package fr.eisti.cergy.jee.service;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.eisti.cergy.jee.model.*;

@Service
@Transactional
public class MonsterServiceImp implements MonsterService{
	
	@Autowired
	private MonstersRepository monsterRepository;

	@Override
	public ArrayList<Monsters> getAll(){
		return (ArrayList<Monsters>) monsterRepository.findAll();
	}

	@Override
	public Monsters getMonsterById(long id) {
		return (Monsters) monsterRepository.findById(id);
	}
}
