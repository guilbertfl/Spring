package fr.eisti.cergy.jee.service.pokemons;

import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.eisti.cergy.jee.model.Pokemons;


@Service
@Transactional
public class PokemonServiceImp implements PokemonService{

	
	@Autowired
	private PokemonsRepository pokemonRepository;
	
	
	@Override
	public Long save(Pokemons pokemon) throws Exception {
		pokemon = pokemonRepository.save(pokemon);
		return pokemon.getId();
	}

	@Override
	public ArrayList<Pokemons> getAll() {
		return (ArrayList<Pokemons>) pokemonRepository.findAll() ;
	}

	@Override
	public Pokemons getById(Long id) throws Exception {
		 return  (Pokemons) pokemonRepository.findById(id);
	}
	
	@Override
	public void delete(Pokemons pokemon){
		pokemonRepository.delete(pokemon);
	}
	
	@Override
	public Long getSequenceId(){
		return pokemonRepository.getNextSerieId();
	}

}
