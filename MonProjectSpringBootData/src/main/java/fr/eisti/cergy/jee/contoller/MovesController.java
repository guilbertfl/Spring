package fr.eisti.cergy.jee.contoller;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import fr.eisti.cergy.jee.model.Moves;
import fr.eisti.cergy.jee.service.moves.MoveService;
 
@Controller("movesController")
public class MovesController {
	
	private final Logger logger = LoggerFactory.getLogger(MonstersController.class);

 
	@Autowired
	MoveService moveService; 
 

	@RequestMapping(value = "/attaque/listAll", method = RequestMethod.GET)
	protected ModelAndView showAllMoves() throws Exception {
		/*
		 * Lancement du Service et recupeation donnees en base
		 */
		ArrayList<Moves> listeMoves = moveService.getAll();
		/*
		 * Envoi Vue + Modele MVC pour Affichage donnees vue
		 */
		return new ModelAndView("/attaque/showAllMoves", "moves", listeMoves);
	}
	
	@RequestMapping(value = "/attaque/get/{id}", method = RequestMethod.GET)
	protected ModelAndView showMoveDetails(@PathVariable String id) throws Exception{
		Moves moveChoisi = null;
		try{
			moveChoisi = moveService.getMoveById(Long.parseLong(id));
		}catch(NullPointerException | NumberFormatException e){
		}
		return new ModelAndView("/attaque/showMoveDetails","moveDetails",moveChoisi);
	}
	
	@RequestMapping(value = "/attaque/get", method = RequestMethod.GET)
	protected ModelAndView showMoverDetails() throws Exception{
		return showMoveDetails("");
	}
}
