package fr.eisti.cergy.jee.service.Dresseur;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;


import fr.eisti.cergy.jee.model.Dresseur;

public interface DresseurRepository extends Repository <Dresseur, Long>{
	
	Dresseur findById(Long id);
	
	Dresseur findByName(String name);

	
	List<Dresseur> findAll();
	
	Dresseur save (Dresseur dresseur);	
	
}
