package fr.eisti.cergy.jee.service.moves;

import java.util.List;

import org.springframework.data.repository.Repository;

import fr.eisti.cergy.jee.model.Monsters;
import fr.eisti.cergy.jee.model.Moves;

public interface MovesRepository extends Repository<Moves, Long>{

	List<Moves> findAll();
	Moves findById(long id);
}
