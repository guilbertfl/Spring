package fr.eisti.cergy.jee.contoller;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import fr.eisti.cergy.jee.model.Abilities;
import fr.eisti.cergy.jee.service.abilities.AbilityService;

@Controller("abilitiesController")
public class AbilitiesController {
	
	private final Logger logger = LoggerFactory.getLogger(MonstersController.class);

	 
	@Autowired
	AbilityService abilityService; 
 

	@RequestMapping(value = "/capacite/listAll", method = RequestMethod.GET)
	protected ModelAndView showAllAbilities() throws Exception {
		/*
		 * Lancement du Service et recupeation donnees en base
		 */
		ArrayList<Abilities> listeAbilities = abilityService.getAll();
		/*
		 * Envoi Vue + Modele MVC pour Affichage donnees vue
		 */
		return new ModelAndView("/capacite/showAllAbilities", "abilities", listeAbilities);
	}
}
