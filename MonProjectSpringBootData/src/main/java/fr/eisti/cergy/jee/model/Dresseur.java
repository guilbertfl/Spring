package fr.eisti.cergy.jee.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "DRESSEUR", schema = "POKEISTI")
public class Dresseur {
	
	private long id;
	private String name;
	private String password;
	
	private Pokemons poke1;
	private Pokemons poke2;
	private Pokemons poke3;
	private Pokemons poke4;
	private Pokemons poke5;
	private Pokemons poke6;
	
	
	public Dresseur(){
	}
	
	public Dresseur(long id, String name, String password){
		this.id = id;
		this.name = name;
		this.password = password;
	}
	
	
	@Id

	@Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
	public long getId() {
		return this.id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	@Column(name = "NAME", nullable = false)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "PASSWORD", nullable = false)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDPOKE1", nullable = false)
	public Pokemons getPoke1() {
		return poke1;
	}

	public void setPoke1(Pokemons poke1) {
		this.poke1 = poke1;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDPOKE2", nullable = false)
	public Pokemons getPoke2() {
		return poke2;
	}

	public void setPoke2(Pokemons poke2) {
		this.poke2 = poke2;
	}
	

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDPOKE3", nullable = false)
	public Pokemons getPoke3() {
		return poke3;
	}

	public void setPoke3(Pokemons poke3) {
		this.poke3 = poke3;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDPOKE4", nullable = false)
	public Pokemons getPoke4() {
		return poke4;
	}

	public void setPoke4(Pokemons poke4) {
		this.poke4 = poke4;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDPOKE5", nullable = false)
	public Pokemons getPoke5() {
		return poke5;
	}

	public void setPoke5(Pokemons poke5) {
		this.poke5 = poke5;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDPOKE6", nullable = false)
	public Pokemons getPoke6() {
		return poke6;
	}

	public void setPoke6(Pokemons poke6) {
		this.poke6 = poke6;
	}

	@Override
	public String toString(){
		return "id : " + this.id  + " name : " + this.name;
	}
	

}
 