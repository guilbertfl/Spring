package fr.eisti.cergy.jee.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Moves generated by hbm2java
 */
@Entity
@Table(name = "MOVES", schema = "POKEISTI")
public class Moves implements java.io.Serializable {

	private long id;
	private Double power;
	private Long pp;
	private Double accuracy;
	private String effect;
	private String type;
	private String moveclass;
	private String name;

	public Moves() {
	}

	public Moves(long id, String type, String moveclass, String name) {
		this.id = id;
		this.type = type;
		this.moveclass = moveclass;
		this.name = name;
	}

	public Moves(long id, double power, Long pp, double accuracy, String effect, String type, String moveclass,
			String name) {
		this.id = id;
		this.power = power;
		this.pp = pp;
		this.accuracy = accuracy;
		this.effect = effect;
		this.type = type;
		this.moveclass = moveclass;
		this.name = name;
	}

	@Id

	@Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "POWER")
	public Double getPower() {
		return this.power;
	}

	public void setPower(Double power) {
		this.power = power;
	}

	@Column(name = "PP", precision = 10, scale = 0)
	public Long getPp() {
		return this.pp;
	}

	public void setPp(Long pp) {
		this.pp = pp;
	}

	@Column(name = "ACCURACY")
	public Double getAccuracy() {
		return this.accuracy;
	}

	public void setAccuracy(Double accuracy) {
		this.accuracy = accuracy;
	}

	@Column(name = "EFFECT")
	public String getEffect() {
		return this.effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	@Column(name = "TYPE", nullable = false)
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "MOVECLASS", nullable = false)
	public String getMoveclass() {
		return this.moveclass;
	}

	public void setMoveclass(String moveclass) {
		this.moveclass = moveclass;
	}

	@Column(name = "NAME", nullable = false)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
