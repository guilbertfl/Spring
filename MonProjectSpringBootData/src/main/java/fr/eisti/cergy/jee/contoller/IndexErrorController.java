package fr.eisti.cergy.jee.contoller;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class IndexErrorController implements ErrorController{
	
    private static final String PATH = "/error";

    @RequestMapping(value = PATH,  method = RequestMethod.GET)
    public String error() {
        return "/404";
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }
}
