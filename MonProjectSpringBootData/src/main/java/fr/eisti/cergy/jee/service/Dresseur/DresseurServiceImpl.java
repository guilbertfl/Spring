package fr.eisti.cergy.jee.service.Dresseur;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.eisti.cergy.jee.model.Dresseur;


@Service
@Transactional
public class DresseurServiceImpl implements DresseurService{

	
	@Autowired
	private DresseurRepository dresseurRepository;
	
	
	@Override
	public Long save(Dresseur dresseur) throws Exception {
		dresseur = dresseurRepository.save(dresseur);
		return dresseur.getId();
	}

	@Override
	public List<Dresseur> getAll() {
		return (List<Dresseur>) dresseurRepository.findAll() ;
	}

	@Override
	public Dresseur getById(Long id) throws Exception {
		 return  (Dresseur) dresseurRepository.findById(id);
	}
	
	@Override
	public Dresseur getByName(String name) throws Exception {
		return (Dresseur) dresseurRepository.findByName(name);
	}


}
